'use strict'
const Promise = require('bluebird');
const jsdom = Promise.promisifyAll(require('jsdom'));

const removeHashtag = (value) => {
    return value.replace('#','')
};

exports.getColors = (urls) => {
    return jsdom.envAsync(
        urls,
        ["http://code.jquery.com/jquery.js"]
    ).then(window => {
        let result = {};
        if(window.$("h1")[0].innerHTML === 'Air ball.')
            throw {message: "invalid page"};
        result.name = window.$("h1")[0].innerHTML;

        let strongTagsArr = window.$("[rel='tag'] > strong");
        let tagsLst = [];
        for (let i = 0; i < strongTagsArr.length; i++) {
            tagsLst.push(strongTagsArr[i].innerHTML);
        }
        result.tags = tagsLst;

        let aTagsArr = window.$("[class='color'] > a");
        let colorsLst = [];
        for (let i = 0; i < aTagsArr.length; i++) {
            colorsLst.push(removeHashtag(aTagsArr[i].innerHTML));
        }
        result.colors = colorsLst;
        return result;
    });
    // return Promise.rejected(new Error('not implemented'))// return promise with array of objects (see readme for more info)
}