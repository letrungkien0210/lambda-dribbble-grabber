'use strict';
const Promise = require('bluebird');
const jsdom = Promise.promisifyAll(require('jsdom'));

jsdom.envAsync(
  "https://dribbble.com/shots/3135922-Prototype",
  ["http://code.jquery.com/jquery.js"]
).then(window => {
  
  let result = {};
  if(window.$("h1")[0].innerHTML === 'Air ball.')
    throw "invalid page";
  result.name = window.$("h1")[0].innerHTML;

  let strongTagsArr = window.$("[rel='tag'] > strong");
  let tagsLst = [];
  for (let i = 0; i < strongTagsArr.length; i++) {
    tagsLst.push(strongTagsArr[i].innerHTML);
  }
  result.tags = tagsLst;

  let aTagsArr = window.$("[class='color'] > a");
  let colorsLst = [];
  for (let i = 0; i < aTagsArr.length; i++) {
    colorsLst.push(aTagsArr[i].innerHTML);
  }
  result.colors = colorsLst;
  
  return result;
}).then(data => console.log(data), err => console.log(err));